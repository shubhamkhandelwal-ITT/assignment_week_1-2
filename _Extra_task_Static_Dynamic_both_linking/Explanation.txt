An extra action  task
In this static linking and Dynamic linking  is performed 
message.h is the header file and it is implemented in meassge.cpp
baisc_operation.h is the header file and it is implemented in basic_operation.cpp
main.cpp is the main file.
To link the library statically and dynamically to the main obeject file first we create object file of each  cpp file
By using : g++ -c main.cpp
           g++ -c message.cpp
	   g++ -c basic_operation.cpp
Then we create static library  of message file by executing this command:
           ar crv lib_message.lib message.o
Then we create dynamic library of basic_operation file by executing this command:
	   g++ -shared -o lib_baisc_operation.dll basic_operation.o
Then we link it with main file by using this command:
           g++ -o main main.o lib_message.lib lib_basic_operation.dll -lm
The executable file main is generated which is independent form  lib_message library.lib and dependent on lib_basic_opertion library.dll