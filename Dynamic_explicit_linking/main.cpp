#include<Windows.h>
#include<iostream>

using namespace std;
typedef int(*add_proc)(int , int);
int main()
{
	HMODULE handle_dll;
	int value_1,value_2,result;
	cout<<"Enter 2 number for addition";
	cin>>value_1>>value_2;
	handle_dll = LoadLibrary(TEXT("C:\\Users\\shubham.khandelwal\\Documents\\For_practice_Bitbucket\\Dynamic_explicit_linking\\math_dll.dll"));/*Explicit Load*/
	if (handle_dll)
	{
		add_proc add =(add_proc) GetProcAddress(handle_dll, "add");
		if (add)
		{
			cout << "add found in dll"<<endl;
			result = add(value_1, value_2); /*Explicit Call*/
			cout << "result is"<<result;
		}
		else
		{
			cout<<"add() not found in math.dll";
		}
		FreeLibrary(handle_dll);
	}
	else
	{
		cout<<"Unable to load math.dll";
	}
	cin.get();
}